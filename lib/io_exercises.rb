# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  computer_number = Random.rand(1..100)
  answer = 101
  num_guesses = 1
  print "Guess a number"
  until answer == computer_number

    chosen_number = gets.chomp
    answer = chosen_number.to_i
    print chosen_number.to_s
    if answer < computer_number
      print "too low"
    elsif answer > computer_number
      print "too high"
    end
    num_guesses += 1
  end
  print  "Number of guesses : " + num_guesses.to_s
end

def file_shuffler
  puts "Give a file name"
  file_name = gets.chomp
  File.open(file_name + "-shuffled.txt","w") do |line|
    line.puts "Yata!!!"
  end

end


if __FILE__ == $PROGRAM_NAME
  file_shuffler
end
